#!/bin/bash

# Create positive images from OPEN hands
while read open_hand;
  do opencv_createsamples -img pos_open_hand/$open_hand -bg neg.txt \
     -info samples_open/$open_hand.txt -num 50 \
     -maxxangle 0.3 -maxyangle 0.3 -maxzangle 0.3 \
     -bgcolor 255 -bgthresh 8 \
     -w 48 -h 48;
done<pos_open_hand/pos_open_hand.txt 

# Create positive images from CLOSED hands
while read closed_hand;
  do opencv_createsamples -img pos_closed_hand/$closed_hand -bg neg.txt \
     -info samples_closed/$closed_hand.txt -num 50 \
     -maxxangle 0.3 -maxyangle 0.3 -maxzangle 0.3 \
     -bgcolor 255 -bgthresh 8 \
     -w 48 -h 48;
done<pos_closed_hand/pos_closed_hand.txt
