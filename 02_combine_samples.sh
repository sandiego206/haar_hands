#!/bin/bash

# Combine description files into one.

cat samples_open/*.txt > samples_open/positives_open.txt
cat samples_closed/*.txt > samples_closed/positives_closed.txt

# Create a .vec file using main description file

opencv_createsamples -info samples_open/positives_open.txt -bg neg.txt -vec samples_open/open.vec -num 8700 -w 48 -h 48

opencv_createsamples -info samples_closed/positives_closed.txt -bg neg.txt -vec samples_closed/closed.vec -num 4850 -w 48 -h 48

