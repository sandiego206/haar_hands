#!/bin/bash

#SBATCH --output=closed_cascades.txt
#SBATCH --time=48:00
#SBATCH --mem-per-cpu=100

opencv_traincascade -data haar_closed -vec samples_closed/closed.vec -bg neg.txt -numPos 114 -numNeg 3019 -numStages 12 -precalcValBufSize 2048 -precalcIdxBufSize 2048 -featureType HAAR -minHitRate 0.995 -maxFalseAlarmRate 0.5 -mode ALL -w 48 -h 48
