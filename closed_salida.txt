PARAMETERS:
cascadeDirName: haar_closed
vecFileName: samples_closed/closed.vec
bgFileName: neg.txt
numPos: 114
numNeg: 3019
numStages: 20
precalcValBufSize[Mb] : 2048
precalcIdxBufSize[Mb] : 2048
stageType: BOOST
featureType: HAAR
sampleWidth: 48
sampleHeight: 48
boostType: GAB
minHitRate: 0.995
maxFalseAlarmRate: 0.5
weightTrimRate: 0.95
maxDepth: 1
maxWeakCount: 100
mode: ALL

Stages 0-12 are loaded

===== TRAINING 13-stage =====
<BEGIN
POS current samples: 1POS current samples: 2POS current samples: 3POS current samples: 4POS current samples: 5POS current samples: 6POS current samples: 7POS current samples: 8POS current samples: 9POS current samples: 10POS current samples: 11POS current samples: 12POS current samples: 13POS current samples: 14POS current samples: 15POS current samples: 16POS current samples: 17POS current samples: 18POS current samples: 19POS current samples: 20POS current samples: 21POS current samples: 22POS current samples: 23POS current samples: 24POS current samples: 25POS current samples: 26POS current samples: 27POS current samples: 28POS current samples: 29POS current samples: 30POS current samples: 31POS current samples: 32POS current samples: 33POS current samples: 34POS current samples: 35POS current samples: 36POS current samples: 37POS current samples: 38POS current samples: 39POS current samples: 40POS current samples: 41POS current samples: 42POS current samples: 43POS current samples: 44POS current samples: 45POS current samples: 46POS current samples: 47POS current samples: 48POS current samples: 49POS current samples: 50POS current samples: 51POS current samples: 52POS current samples: 53POS current samples: 54POS current samples: 55POS current samples: 56POS current samples: 57POS current samples: 58POS current samples: 59POS current samples: 60POS current samples: 61POS current samples: 62POS current samples: 63POS current samples: 64POS current samples: 65POS current samples: 66POS current samples: 67POS current samples: 68POS current samples: 69POS current samples: 70POS current samples: 71POS current samples: 72POS current samples: 73POS current samples: 74POS current samples: 75POS current samples: 76POS current samples: 77POS current samples: 78POS current samples: 79POS current samples: 80POS current samples: 81POS current samples: 82POS current samples: 83POS current samples: 84POS current samples: 85POS current samples: 86POS current samples: 87POS current samples: 88POS current samples: 89POS current samples: 90POS current samples: 91POS current samples: 92POS current samples: 93POS current samples: 94POS current samples: 95POS current samples: 96POS current samples: 97POS current samples: 98POS current samples: 99POS current samples: 100POS current samples: 101POS current samples: 102POS current samples: 103POS current samples: 104POS current samples: 105POS current samples: 106POS current samples: 107POS current samples: 108POS current samples: 109POS current samples: 110POS current samples: 111POS current samples: 112POS current samples: 113POS current samples: 114POS count : consumed   114 : 114
srun: Force Terminated job 275
srun: Job step aborted: Waiting up to 32 seconds for job step to finish.
slurmstepd: *** STEP 275.0 CANCELLED AT 2015-09-10T14:10:08 *** on guane01
srun: error: guane01: task 0: Terminated
