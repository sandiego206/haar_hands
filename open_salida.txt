PARAMETERS:
cascadeDirName: haar_open
vecFileName: samples_open/open.vec
bgFileName: neg.txt
numPos: 204
numNeg: 3019
numStages: 20
precalcValBufSize[Mb] : 2048
precalcIdxBufSize[Mb] : 2048
stageType: BOOST
featureType: HAAR
sampleWidth: 48
sampleHeight: 48
boostType: GAB
minHitRate: 0.995
maxFalseAlarmRate: 0.5
weightTrimRate: 0.95
maxDepth: 1
maxWeakCount: 100
mode: ALL

Stages 0-11 are loaded

===== TRAINING 12-stage =====
<BEGIN
POS current samples: 1POS current samples: 2POS current samples: 3POS current samples: 4POS current samples: 5POS current samples: 6POS current samples: 7POS current samples: 8POS current samples: 9POS current samples: 10POS current samples: 11POS current samples: 12POS current samples: 13POS current samples: 14POS current samples: 15POS current samples: 16POS current samples: 17POS current samples: 18POS current samples: 19POS current samples: 20POS current samples: 21POS current samples: 22POS current samples: 23POS current samples: 24POS current samples: 25POS current samples: 26POS current samples: 27POS current samples: 28POS current samples: 29POS current samples: 30POS current samples: 31POS current samples: 32POS current samples: 33POS current samples: 34POS current samples: 35POS current samples: 36POS current samples: 37POS current samples: 38POS current samples: 39POS current samples: 40POS current samples: 41POS current samples: 42POS current samples: 43POS current samples: 44POS current samples: 45POS current samples: 46POS current samples: 47POS current samples: 48POS current samples: 49POS current samples: 50POS current samples: 51POS current samples: 52POS current samples: 53POS current samples: 54POS current samples: 55POS current samples: 56POS current samples: 57POS current samples: 58POS current samples: 59POS current samples: 60POS current samples: 61POS current samples: 62POS current samples: 63POS current samples: 64POS current samples: 65POS current samples: 66POS current samples: 67POS current samples: 68POS current samples: 69POS current samples: 70POS current samples: 71POS current samples: 72POS current samples: 73POS current samples: 74POS current samples: 75POS current samples: 76POS current samples: 77POS current samples: 78POS current samples: 79POS current samples: 80POS current samples: 81POS current samples: 82POS current samples: 83POS current samples: 84POS current samples: 85POS current samples: 86POS current samples: 87POS current samples: 88POS current samples: 89POS current samples: 90POS current samples: 91POS current samples: 92POS current samples: 93POS current samples: 94POS current samples: 95POS current samples: 96POS current samples: 97POS current samples: 98POS current samples: 99POS current samples: 100POS current samples: 101POS current samples: 102POS current samples: 103POS current samples: 104POS current samples: 105POS current samples: 106POS current samples: 107POS current samples: 108POS current samples: 109POS current samples: 110POS current samples: 111POS current samples: 112POS current samples: 113POS current samples: 114POS current samples: 115POS current samples: 116POS current samples: 117POS current samples: 118POS current samples: 119POS current samples: 120POS current samples: 121POS current samples: 122POS current samples: 123POS current samples: 124POS current samples: 125POS current samples: 126POS current samples: 127POS current samples: 128POS current samples: 129POS current samples: 130POS current samples: 131POS current samples: 132POS current samples: 133POS current samples: 134POS current samples: 135POS current samples: 136POS current samples: 137POS current samples: 138POS current samples: 139POS current samples: 140POS current samples: 141POS current samples: 142POS current samples: 143POS current samples: 144POS current samples: 145POS current samples: 146POS current samples: 147POS current samples: 148POS current samples: 149POS current samples: 150POS current samples: 151POS current samples: 152POS current samples: 153POS current samples: 154POS current samples: 155POS current samples: 156POS current samples: 157POS current samples: 158POS current samples: 159POS current samples: 160POS current samples: 161POS current samples: 162POS current samples: 163POS current samples: 164POS current samples: 165POS current samples: 166POS current samples: 167POS current samples: 168POS current samples: 169POS current samples: 170POS current samples: 171POS current samples: 172POS current samples: 173POS current samples: 174POS current samples: 175POS current samples: 176POS current samples: 177POS current samples: 178POS current samples: 179POS current samples: 180POS current samples: 181POS current samples: 182POS current samples: 183POS current samples: 184POS current samples: 185POS current samples: 186POS current samples: 187POS current samples: 188POS current samples: 189POS current samples: 190POS current samples: 191POS current samples: 192POS current samples: 193POS current samples: 194POS current samples: 195POS current samples: 196POS current samples: 197POS current samples: 198POS current samples: 199POS current samples: 200POS current samples: 201POS current samples: 202POS current samples: 203POS current samples: 204POS count : consumed   204 : 252
srun: Force Terminated job 274
srun: Job step aborted: Waiting up to 32 seconds for job step to finish.
slurmstepd: *** STEP 274.0 CANCELLED AT 2015-09-10T14:10:03 *** on guane01
srun: error: guane01: task 0: Terminated
